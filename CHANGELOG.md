# EOQ2 Python Library - Changelog

## 2.2.17.2 - 2024-06-02
* small update preventing regex failures with newer python versions, i.e. \s is unrecognized token

## 2.2.17 - 2023-12-12
* replaced imp by importlib, because imp is gone with python 3.12

## 2.2.16 - 2023-05-24
* Fixed inconsitency for callManager in CmdRunnerBasedDomain
* Added function to specify a default session ID for CmdRunnerBasedDomain

## 2.2.15 - 2023-04-14
* added support for pre-compiled actions, i.e. pyc files.

## 2.2.14 - 2022-09-13
* fixed error when getting multi-element attributes, that excluded false-like values as 0 or False to be returned.

## 2.2.13 - 2022-08-27
* fixed error when working with multi element attributes, which are not unique

## 2.2.11 - 2021-11-16
* fixes pyecore incompatibility causing an empty model from pyecore version 0.12.1 on.
* fixes deadlock when resolving proxies introduced with eoq v2.2.9

## 2.2.8 - 2021-06-25
* major bug fixing in text parser and text serializer: several variants of nested queries, nested arrays and lists as well as their combination failed or resulted in wrong statements when serialized to text and parsed again.
* Resolved ambiguity between array query [] and list (was []) is now () 
* Changed command serialization and parsing from CMD(arg1,arg2,...) to CMD arg1 arg2 ...
* Added a basic string representation to command objects
* Reserved % as the null primitive. Cross product is now &CSP

## 2.2.7 - 2021-06-15
* allowed queries as default argument for TRY
* allowed SEL as a start segment in queries
* changed serialization of CMP commands to newline instead of ;

## 2.2.6 - 2021-03-26
* Fixed CHG command: Will now alway return a list. If however, changeId or n are out of range of the stored changes, [] is returned.
* Fixed textserializer issues:
  * Lists are now serialized with [] instead of ()
  * Commands with no arguments are now deserialised correctly
  * Simpified deserialization of compound commands
* Added Mute (MUT) and Unmute (UMT) command, wich can be used to supress results of certian statements of compound commands
* Extended the change event to also include user name and session number of the user causing the change: field 9 and 11
* Removed unwanted dict in return value of CAL's result's outputs.
* Increased default value for the maximum number of changes stored from 100 to 10000. 

## 2.2.5 - 2021-03-22
* Added prototype of CPR and MRG commands for testing purpose 

## 2.2.4 - 2021-03-22
* Fixed: Text parser repaired for queries starting with &TRY

## 2.2.3
* Fixed: Proxy resolving error in MET ASSOCIATES query

## 2.2.2
* Fixed: ID-based references between XMI files are now loaded correctly.

## 2.2.1
* Added TRY query segment, which can be used to return a default value if a query fails for any reason

## 2.2.0
* Added RGX command, which evaluates regular expressions on strings. The result is true or false. Can be used in selectors.

## 2.1.8
* Added trackFileChanges parameter of pyecore workspace MDB. If set to false file change tracking is disabled. The default is true.

## 2.1.7

* Added tracking of file changes: While EOQ runs any file change in the workspace directory that is either a known model resource, a meta model or a folder will be captured and updated in the model tree.